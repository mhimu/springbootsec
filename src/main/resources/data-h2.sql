-- seed the database

-- default users and roles

insert into users values
	(1, 'user', '$2b$10$ovx5lBKmIn7eT4BdUlqYwOV3jnvD8GLeCf6ZxRg7oJzDLRFSZ2jpq', 'N', 'Y', null, null);
insert into users values
	(2, 'admin', '$2b$10$ovx5lBKmIn7eT4BdUlqYwOV3jnvD8GLeCf6ZxRg7oJzDLRFSZ2jpq', 'N', 'Y', null, null);

insert into roles values (1, 'ROLE_USER');
insert into roles values (2, 'ROLE_USER');
insert into roles values (2, 'ROLE_ADMIN');
