-- initialize the database

-- users and role tables tables for storing login and authorizations

create table users
(
	id bigint primary key,
	username varchar(25) not null,
	password char(60) not null,
	locked char(1) not null,
	enabled char(1) not null,
	expiry_date date null,
	password_expiry_date date null
);

create table roles
(
	userid bigint not null,
	rolename varchar(25) not null,
	primary key (userid, rolename)
);
