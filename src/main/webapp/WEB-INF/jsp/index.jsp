<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <title>Spring Boot Security Demo</title>
</head>
<body>
    <h2>Spring Boot Security</h2>
    <p>Hello, ${pageContext.request.userPrincipal.name}, the date is ${today}.</p>
    <p>Following are some security-related info using JSP EL (from Request object):</p>
    <ul>
    	<li>pageContext.request.userPrincipal.name: ${pageContext.request.userPrincipal.name}</li>
    	<li>pageContext.request.remoteUser: ${pageContext.request.remoteUser}</li>
    </ul>
    <p>Following are some security-related info using Spring Security Taglib</p>
	<sec:authentication property="principal" var="prin"/>
	<sec:authentication property="authorities" var="auths"/>
	<ul>
		<li>sec:authentication->authenticated: <sec:authentication property="authenticated"/></li>
		<li>sec:authentication->principal: ${prin}</li>
		<li>sec:authentication->authorities:
			<ul>
			<c:forEach items="${auths}" var="auth">
				<li>${auth}</li>
			</c:forEach>
			</ul>
		</li>
		<li>sec:authentication->details.remoteAddress: <sec:authentication property="details.remoteAddress"/></li>
		<li>sec:authentication->details.sessionId: <sec:authentication property="details.sessionId"/></li>
	</ul>
    <hr>
    <p><b>What would you like to do?</b></p>
    <ul>
        <li><a href="${pageContext.request.contextPath}/book">Go to Books Page</a></li>
        <li><a href="${pageContext.request.contextPath}/admin">Go to Administration</a></li>
    </ul>
    <sec:authorize access="hasRole('ADMIN')">
    <p><b>Only users having ADMIN role can view this content.</b></p>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
    <a href="${pageContext.request.contextPath}/logout">[[Logout]]</a>
    </sec:authorize>
</body>
</html>