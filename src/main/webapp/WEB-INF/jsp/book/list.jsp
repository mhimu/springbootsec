<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book List</title>
</head>

<body>
    <h2>List of Books</h2>
    <hr>
    <ol>
    <c:forEach items="${books}" var="book">
        <li>${book.title} by ${book.author}</li>
    </c:forEach>
    </ol>
    <hr>
    <a href="${pageContext.request.contextPath}/book/add">Add a new book</a><br>
    <a href="${pageContext.request.contextPath}/book">Back to Books page</a>
</body>
</html>