<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add New Book</title>
</head>
<body>
	<h2>Add New Book</h2>
	<hr>
	<form:form method="post">
	<table>
		<tr>
			<td><label for="title">Title</label></td>
			<td><input type="text" name="title" id="title" placeholder="Book Title"></td>
		</tr>
		<tr>
			<td><label for="author">Author</label></td>
			<td><input type="text" name="author" id="author" placeholder="Author Name(s)"></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="reset"> <input type="submit" value="Add"></td>
		</tr>
	</table>
	</form:form>
 	<hr>
	<a href="${pageContext.request.contextPath}/book">Back to Books page</a>
</body>
</html>