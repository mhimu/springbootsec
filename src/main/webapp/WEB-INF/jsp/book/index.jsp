<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Books</title>
</head>
<body>
    <h2>Books [<a href="/">Home</a>]</h2>
    <hr>
    <ul>
        <li><a href="${pageContext.request.contextPath}/book/list">List books</a></li>
        <li><a href="${pageContext.request.contextPath}/book/add">Add a new book</a></li>
    </ul>
</body>
</html>