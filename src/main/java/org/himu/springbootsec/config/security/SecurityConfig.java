package org.himu.springbootsec.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//	@Autowired private CustomUserDetailsService customUserDetailsService;
	@Autowired private CustomAuthenticationProvider customAuthenticationProvider;

// 	@Override
// 	public void configure(WebSecurity web) throws Exception {
// 		web
// 			.ignoring()
// 				// Spring Security should completely ignore URLs starting with /resources/
// 				.antMatchers("/resources/**");
// 	}

	@Override
 	protected void configure(HttpSecurity http) throws Exception {
 		http
		 	// we're setting up authorization
		 	.authorizeRequests()
				// only ADMIN role can access admin page and anything under this path
				.antMatchers("/admin/**").hasAnyRole("ADMIN")
				// for any other page, the user must be authorized, having either of the roles USER or ADMIN
				.antMatchers("/**").hasAnyRole("USER", "ADMIN")
				// AND we want form-based login (instead of HTTP Basic), this being accessible to anyone (permitAll)
				.and()
				.formLogin().permitAll();
 	}

// 	@Override
// 	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
// 		auth
//	 	// use an in-memory authentication manager that is pre-supplied with Spring Security
//			.inMemoryAuthentication()
//		 	// add a user named 'user' having a password of 'password' having a role 'USER'
//		 	.withUser("user").password("password").roles("USER")
//			.and()
//			// another user named 'admin' having a password of 'password' having a roles 'USER' & 'ADMIN'
//		 	.withUser("admin").password("password").roles("USER", "ADMIN")
//			.and()
//			// we have to supply a password encoder - just getting away with a NoOp
//			.passwordEncoder(NoOpPasswordEncoder.getInstance());
// 	} 	
	
 	@Override
 	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
 		auth
 			.authenticationProvider(customAuthenticationProvider);
 			// use our 
// 			.userDetailsService(customUserDetailsService)
// 			.passwordEncoder(new BCryptPasswordEncoder());
 	}
 	
 	@Bean
 	public PasswordEncoder passwordEncoder() {
 		return new BCryptPasswordEncoder();
 	}
}
