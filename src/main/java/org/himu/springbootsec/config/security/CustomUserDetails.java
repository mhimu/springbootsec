package org.himu.springbootsec.config.security;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

import org.himu.springbootsec.domain.security.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetails implements UserDetails {

	private static final long serialVersionUID = 1L;

	private User user;

    public CustomUserDetails() {
    }

    public CustomUserDetails(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getRoles()
            .stream()
            .map(r -> new SimpleGrantedAuthority(r.getRolename()))
            .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.getExpiryDate() == null ? true : user.getExpiryDate().isBefore(LocalDate.now());
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.getLocked().compareTo("N") == 0;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.getPasswordExpiryDate() == null ? true : user.getPasswordExpiryDate().isBefore(LocalDate.now());
    }

    @Override
    public boolean isEnabled() {
        return user.getEnabled().compareTo("Y") == 0;
    }

}
