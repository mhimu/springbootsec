package org.himu.springbootsec.config.security;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.himu.springbootsec.dao.UserRepository;
import org.himu.springbootsec.domain.security.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static Logger log = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
	
	@Autowired private UserRepository userRepository;
	
	@Autowired private PasswordEncoder passwordEncoder;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		Object credentials = authentication.getCredentials();		
	
		log.info("username: " + username);
		log.info("credentials type: " + credentials.getClass());

		log.info("retrieving user...");
        Optional<User> user = userRepository.findByUsername(username);
        if (!user.isPresent()) {
        	throw new UsernameNotFoundException("Not Found");
        }
        
        log.info("verifying password...");
        // make sure to use the matches() method and not use any sort of comparison
        if (!passwordEncoder.matches(credentials.toString(), user.get().getPassword())) {
        	throw new BadCredentialsException("Bad Credentials");
        }
        
        List<GrantedAuthority> authorities =
        		user.get().getRoles()
        			.stream()
        			.map(r -> new SimpleGrantedAuthority(r.getRolename()))
        			.collect(Collectors.toList());
        
        UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
        		authentication.getPrincipal(), authentication.getCredentials(), authorities);
        result.setDetails(authentication.getDetails());
        
		return result;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
