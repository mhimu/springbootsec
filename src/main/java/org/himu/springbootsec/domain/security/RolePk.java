package org.himu.springbootsec.domain.security;

import java.io.Serializable;

public class RolePk implements Serializable {

	private static final long serialVersionUID = 1L;

	private int userid;
    private String rolename;

    public RolePk () {
    }

    public RolePk(int userid, String rolename) {
        this.userid = userid;
        this.rolename = rolename;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

}
