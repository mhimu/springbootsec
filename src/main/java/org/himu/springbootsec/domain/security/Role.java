package org.himu.springbootsec.domain.security;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
@IdClass(RolePk.class)
public class Role {

    @Id
    private int userid;
    @Id
    private String rolename;

    public Role () {
    }

    public Role(int userid, String rolename) {
        this.userid = userid;
        this.rolename = rolename;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

}
