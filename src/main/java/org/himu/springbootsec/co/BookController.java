package org.himu.springbootsec.co;

import org.himu.springbootsec.domain.Book;
import org.himu.springbootsec.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired BookService bookService;

    @GetMapping("")
    public String index() {
        return "book/index";
    }

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("books", bookService.getBookList());
        return "book/list";
    }

    @GetMapping("/add")
    public String addGet() {
    	return "book/add";
    }
    
    @PostMapping("/add")
    public String add(Book book, Model model) {
        if (book != null)
            bookService.addNewBook(book);
        model.addAttribute("books", bookService.getBookList());
        return "book/list";
    }
    
}