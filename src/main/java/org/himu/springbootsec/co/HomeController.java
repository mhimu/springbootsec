package org.himu.springbootsec.co;

import java.time.LocalDate;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    
    @GetMapping("/")
    public String index(Model model, SecurityContextHolder holder) {
        model.addAttribute("today", LocalDate.now());
        return "index";
    }
}