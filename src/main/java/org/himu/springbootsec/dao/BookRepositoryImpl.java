package org.himu.springbootsec.dao;

import java.util.ArrayList;
import java.util.List;

import org.himu.springbootsec.domain.Book;
import org.springframework.stereotype.Component;

@Component
public class BookRepositoryImpl implements BookRepository {

    private List<Book> books = new ArrayList<>();

    public BookRepositoryImpl() {
        books.add(new Book("Data Smart", "John W. Foreman"));
        books.add(new Book("HTML5 & CSS3 for the Real World", "Goldstein, Lazaris, Weyl"));
        books.add(new Book("Programming in Python 3","Mark Summerfield"));
    }

    @Override
    public Iterable<Book> findAll() {
        return books;
    }

    @Override
    public void save(Book book) {
        books.add(book);
    }
    
}