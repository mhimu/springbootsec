package org.himu.springbootsec.dao;

import java.util.Optional;

import org.himu.springbootsec.domain.security.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	// Spring Data JPA automatically creates the implementation
	// see https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
    public Optional<User> findByUsername(String username);

}
