package org.himu.springbootsec.dao;

import org.himu.springbootsec.domain.Book;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository {
    Iterable<Book> findAll();
    void save(Book book);
}