package org.himu.springbootsec.service;

import org.himu.springbootsec.dao.BookRepository;
import org.himu.springbootsec.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
    
    @Autowired BookRepository bookRepository;

    public Iterable<Book> getBookList() {
        return bookRepository.findAll();
    }

    public void addNewBook(Book book) {
        bookRepository.save(book);
    }
}